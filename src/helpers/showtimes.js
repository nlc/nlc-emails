var $ = require('cheerio');
var dateFormat = require('dateFormat');


function formatShowtime(showtime) {
  var datestr = showtime.PreShowStartTime;
  var tzOffset = null;

  var d = new Date(Date.parse(datestr + "Z"));
  var fDate = dateFormat(d, "dddd, mmmm dS, yyyy");
  var fTime = ((d.getUTCHours() % 12) || 12) + ":" + ("00" + d.getUTCMinutes()).slice(-2) + ((d.getUTCHours() >= 12) ? "pm" : "am");
  var timeText = fDate + ", " + fTime;

  if (showtime.SeatsAvailable == 0) {
    var $container = $("<span>");
    timeText += " [SOLD OUT]";
  } else {
    var $container = $("<a>")
      .attr("target", "_blank")
      .attr("href", showtime.Url);

    if (showtime.SeatsAvailable <= 10) {
      timeText += " -- Buy Now! Only " + showtime.SeatsAvailable + " Seats Left!";
    }
  }

  return $container.text(timeText);
}

// Example file src/helpers/showtimes.js
module.exports = function(options) {
  var allShowtimes = options.hash.data;
  var sessions = options.hash.sessions.split(",");
  var selectedShowtimes = [];
  var dates = {};

  for (var i = 0; i < sessions.length; i++) {
    var session = sessions[i];
    var sessionShowtimes = allShowtimes[session];
    Array.prototype.push.apply(selectedShowtimes, sessionShowtimes);
  }

  selectedShowtimes.sort(function(a, b) {
    var aDate = dates[a.PreShowStartTime] = dates[a.PreShowStartTime] || new Date(a.PreShowStartTime);
    var bDate = dates[b.PreShowStartTime] = dates[b.PreShowStartTime] || new Date(b.PreShowStartTime);
    return (aDate > bDate) ? 1 : (aDate < bDate ? -1 : 0);
  });

  var $outer = $("<div>");
  var $container = $("<p>").addClass("showtimes").appendTo($outer);

  for (var i = 0; i < selectedShowtimes.length; i++) {
    var showtime = selectedShowtimes[i];
    $container.append(formatShowtime(showtime));
    $container.append($("<br>"));
  }

  return $outer.html();
}
