var http = require('http');
var jsonfile = require('jsonfile');


module.exports = function(done) {
  var getAllShowtimes = (function() {
    var allShowtimes = null;

    return function(cb) {
      if (!allShowtimes) {
        http.get({
          hostname : "api.us.veezi.com",
          path : "/v1/websession",
          headers : {
            Accept : "",
            Host : "api.us.veezi.com",
            VeeziAccessToken : "Hxi80VjpWU2GxfO2iG493Q2"
          }
        }, (res) => {
          var data = "";

          res.on("data", (chunk) => {
            data += chunk;
          });

          res.on("end", () => {
            var rawShowtimes = JSON.parse(data);
            allShowtimes = {};
            for (var i = 0; i < rawShowtimes.length; i++) {
              var rawShowtime = rawShowtimes[i];
              var filmShowtimes = allShowtimes[rawShowtime.FilmId] = allShowtimes[rawShowtime.FilmId] || [];
              filmShowtimes.push(rawShowtime);
            }

            setImmediate(function() { cb(allShowtimes); });
          });
        }).on("error", (e) => {
          console.error(e);
        });
      }
      else {
        setImmediate(function() { cb(allShowtimes); });
      }
    }
  })();

  getAllShowtimes(function(showtimes) {
    jsonfile.writeFileSync("./src/data/showtimes.json", showtimes);
    done();
  });
}
