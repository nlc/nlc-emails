import inky from 'inky';

var $ = require('cheerio');
var http = require('http');
var format = require('util').format;
var extend = require('util')._extend;
var values = require('object-values');
var base = inky.Inky.prototype.componentFactory;
var installed = false;

module.exports = function(element) {
  if (!installed) {
    this.components = extend(this.components, {
      movie : "movie",
      by : "by",
      showtimes : "showtimes"
    });
    this.componentTags = values(this.components);
    installed = true;
  }

  switch (element[0].name) {
    case this.components.movie:
      return format('<em>%s</em>', element.html());
      break;
    case this.components.by:
      var nameAttr = element.attr("name");
      var outletAttr = element.attr("outlet");
      var linkAttr = element.attr("link") || "";
      var $link = $("<a>").attr("href", linkAttr);
      var $p = $("<p>").css({"text-align" : "right"});
      $p.append("~~ ");

      if (nameAttr) {
        if (linkAttr && !outletAttr) {
          $p.append($link.text(nameAttr));
        } else {
          $p.append(nameAttr)
        }
      }

      if (outletAttr) {
        if (nameAttr) {
          $p.append(", ");
        }
        if (linkAttr) {
          $p.append($link.append(outletAttr));
        } else {
          $p.append(outletAttr);
        }
      }

      return $("<p>").append($p).html();
      break;
    case this.components.showtimes:
      return "SHOWTIMES!";
      break;
    default:
      return base.call(this, element);
  }
}
